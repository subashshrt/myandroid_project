package com.example.helloandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button submitButton = (Button) findViewById(R.id.submitButton);
        Button subButton  =(Button) findViewById(R.id.subButton);
        Button goToFacebook =(Button) findViewById(R.id.goToFacebook);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstNumber = (EditText) findViewById(R.id.firstNumber);
                EditText secondNumber = (EditText) findViewById(R.id.secondNumber);
                TextView result = (TextView) findViewById(R.id.result);

                int num1= Integer.parseInt(firstNumber.getText().toString());
                int num2= Integer.parseInt(secondNumber.getText().toString());
                int res= num1+num2;
                result.setText(  res+" is your answer");
            }
        });
        subButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstNumber = (EditText) findViewById(R.id.firstNumber);
                EditText secondNumber = (EditText) findViewById(R.id.secondNumber);
                TextView result = (TextView) findViewById(R.id.result);

                int num1= Integer.parseInt(firstNumber.getText().toString());
                int num2= Integer.parseInt(secondNumber.getText().toString());
                int res= num1-num2;
                result.setText(  res+" is your answer");
            }
        });
        goToFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 setContentView(R.layout.facebook);
            }
        });


    }



    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Welcome to calculator", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Welcome back to calculator", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Application paused", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "You are out of calculator", Toast.LENGTH_LONG).show();
    }
}
