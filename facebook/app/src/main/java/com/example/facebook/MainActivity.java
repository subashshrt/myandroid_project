package com.example.facebook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnSignup ;
    int temp;
    TextView firstname , lastname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSignup= (Button) findViewById(R.id.btn_signup);
        firstname = (TextView) findViewById(R.id.et_firstname);
        lastname =(TextView) findViewById(R.id.et_secondname) ;
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp +=1;
                Toast.makeText(MainActivity.this, "You have clicked the signin button \n"+temp+"\n times  MR" + firstname +lastname, Toast.LENGTH_LONG).show();
                System.out.println("This is facebook sign page");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Welcome to Facebook", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Welcocme back to facebook", Toast.LENGTH_LONG).show();
    }
}
